### Code test project ###

The purpose is to get started on a simple project and set up a simple proof of concept implementation

### Project description ###

Set up a simple single page web application that

* Shows the **real time** stock price for TEAM (Atlassian) and your favourite company (Microsoft?) on the page.
* Refreshes the stock price display at least once a minute
* Indicates the trend of the price: falling, flat, rising

### Technical requirements ###

Use the following technology options

* NPM
* Nodejs
* React/Typescript
* Atlaskit (UI) (optional)
* A JS testing framework

### Tasks ###

Within 2h complete as many of the following tasks as possible 

* Fork the repo
* Bootstrap the single page application
* Implement POC with React/Typescript
* Provide unit tests for components 

### Delivery ###

After 2h

* Create a pull request.
* Code review walk-through the solution
* Demo the application

The Izymes team wishes you good luck and happy coding!
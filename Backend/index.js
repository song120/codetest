require('dotenv').config() // .env file support for configuration

const axios = require('axios');
const express = require('express')
const app = express()
const cors = require('cors')
const logger = require('./controllers/logger.controller').logger
const swaggerUI = require ('swagger-ui-express')
const swaggerJsDoc = require ('swagger-jsdoc')
const routes = require('./routes/router');

const swaggerOption = require ('./swagger')
const jsDoc = swaggerJsDoc (swaggerOption)

app.use ('/swagger', swaggerUI.serve, swaggerUI.setup(jsDoc))

const EXPRESS_PORT = process.env.EXPRESS_PORT || 3000

// middlewares
app.use(cors());                // allow cors origin
app.use(express.json());        // accept json
app.use('/api', routes);         // route handler for '/cats'

// get stock price
app.get("/", async (req, res, next) => {
    const response = await axios.get("https://finnhub.io/api/v1/stock/symbol?exchange=US&token=c8s49hiad3idbo5bhpb0")
    try {
        const microsoft = response.data.find(el => el.description == 'MICROSOFT CORP');
        // const atlans = response.data.find(el => el.description == 'ATLASSIAN CORP PLC-CLASS A');
        // const price = await axios.get(`https://finnhub.io/api/v1/quote?symbol=${microsoft.symbol}&token=c8s49hiad3idbo5bhpb0`)
        const from  = new Date("2022-03-20 00:00:00").getTime();
        const to  = new Date("2022-03-21 23:59:59").getTime();
        console.log(from, to)
        const price = await axios.get(`https://finnhub.io/api/v1/stock/candle?symbol=${microsoft.symbol}&resolution=1&from=${from}&to=${to}&token=c8s49hiad3idbo5bhpb0`)
        console.log(price.data)
        res.send(price.data);
    } catch (err) {
        console.log(err)
    }
})

// start app
app.listen(EXPRESS_PORT, () => console.log(`Example app listening on port ${EXPRESS_PORT}!`));
// logger.info(`Example app listening on port ${EXPRESS_PORT}!`);